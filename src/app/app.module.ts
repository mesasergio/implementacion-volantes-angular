import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OpcionesInicioComponent } from './pages/opciones-inicio/opciones-inicio.component';
import { EnvioCorreoComponent } from './pages/envio-correo/envio-correo.component';
import { ImpresionVolanteComponent } from './pages/impresion-volante/impresion-volante.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatInputModule, MatSelectModule, MatRadioModule, MatCardModule } from '@angular/material';
import { NavbarMaterialComponent } from './components/navbar-material/navbar-material.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FormularioEnvioComponent } from './components/formulario-envio/formulario-envio.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FormularioImpresionComponent } from './components/formulario-impresion/formulario-impresion.component';

@NgModule({
  declarations: [
    AppComponent,
    OpcionesInicioComponent,
    EnvioCorreoComponent,
    ImpresionVolanteComponent,
    NavbarMaterialComponent,
    FormularioEnvioComponent,
    FormularioImpresionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTooltipModule,
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatProgressBarModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
