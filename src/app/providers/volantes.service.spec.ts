import { TestBed } from '@angular/core/testing';

import { VolantesService } from './volantes.service';

describe('VolantesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VolantesService = TestBed.get(VolantesService);
    expect(service).toBeTruthy();
  });
});
