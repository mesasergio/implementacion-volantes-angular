import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

const base = "https://us-central1-sri-volante.cloudfunctions.net/registroPropiedad";
const baseImpresion = "https://us-central1-sri-volante.cloudfunctions.net/propiedad";


@Injectable({
  providedIn: 'root'
})
export class VolantesService {

  constructor(
    private http: HttpClient
  ) { }

  enviarInformacion(body):Promise<any>{

    return new Promise((resolve,reject)=>{
      this.http.post(base, body,{
        headers: { "Content-Type": "application/json"}
      }).subscribe((resp:any)=>{
        resolve(resp);
      },error=>{
        console.log(error);
        reject(error);
      })
    })
  }

  imprimirInformacion(body): Promise<any> {

    return new Promise((resolve, reject) => {
      this.http.post(base, body, {
        headers: { "Content-Type": "application/json" }
      }).subscribe((resp: any) => {
        this.impresion(body.codigo).then((respHtml)=>{
          resolve(respHtml);
        }).catch(error=>{
          reject(error);
        })
      }, error => {
        reject(error);
      })
    })
  }

  impresion(codigo){

    return new Promise((resolve, reject) => {

      var xhr = new XMLHttpRequest();
      let url = baseImpresion + `?codigo=${codigo}`;
      xhr.onload = function () {
        let objecthtml = new XMLSerializer().serializeToString(this.responseXML);
        resolve(objecthtml);
      }
      xhr.onerror = function () {
        reject('<p>Pagina no existe</p>')
      }

      xhr.open("GET", url, true);
      xhr.responseType = "document";
      xhr.send();
    })
  }
}
