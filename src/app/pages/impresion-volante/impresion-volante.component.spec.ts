import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImpresionVolanteComponent } from './impresion-volante.component';

describe('ImpresionVolanteComponent', () => {
  let component: ImpresionVolanteComponent;
  let fixture: ComponentFixture<ImpresionVolanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImpresionVolanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImpresionVolanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
