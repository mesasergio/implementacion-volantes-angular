import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpcionesInicioComponent } from './opciones-inicio.component';

describe('OpcionesInicioComponent', () => {
  let component: OpcionesInicioComponent;
  let fixture: ComponentFixture<OpcionesInicioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpcionesInicioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpcionesInicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
