import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioImpresionComponent } from './formulario-impresion.component';

describe('FormularioImpresionComponent', () => {
  let component: FormularioImpresionComponent;
  let fixture: ComponentFixture<FormularioImpresionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioImpresionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioImpresionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
