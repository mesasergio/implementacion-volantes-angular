import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { VolantesService } from 'src/app/providers/volantes.service';
import { MatSnackBar } from '@angular/material';



@Component({
  selector: "app-formulario-envio",
  templateUrl: "./formulario-envio.component.html",
  styleUrls: ["./formulario-envio.component.scss"]
})
export class FormularioEnvioComponent {
  formDisable = false;

  infoForm = this.fb.group({
    codigo: ["99", Validators.required],
    imgHeader: [
      "http://s3.amazonaws.com/srinmobiliario-dev/images/files/2/medium-00033FDF-11-088a77052a8a4aa2a253704fc5eb940a.jpg?1529421457",
      Validators.required
    ],
    titulo: ["Casa", Validators.required],
    ubicacion: ["El tesoro", Validators.required],
    precio: ["1260'000.000", Validators.required],
    area: ["398", Validators.required],
    habitaciones: ["4", Validators.required],
    banos: ["5", Validators.required],
    areas: [["Zonas verdes", "Salón común", "Ascensor"], Validators.required],
    parqueaderos: ["2", Validators.required],
    administracion: ["1'118.000", Validators.required],
    predial: ["650.000", Validators.required],
    caracteristicas: [
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur feugiat nisi eu dolor posuere, pretium aliquam lectus dapibus. Suspendisse rhoncus nisl quis fermentum tempus.",
      Validators.required
    ],
    imgAsesor: [
      "https://firebasestorage.googleapis.com/v0/b/sri-volante.appspot.com/o/foto-asesor.jpg?alt=media&token=66b8a368-bada-493f-bf31-c05a4000c75c",
      Validators.required
    ],
    nombreAsesor: ["Cristian Herrera", Validators.required],
    fraseAsesor: ["SRI", Validators.required],
    telefonoAsesor: ["(57) 315 494 9716", Validators.required],
    correoAsesor: ["cherrera@srinmobiliario.com", Validators.required],
    email: ["mesasergio@gmail.com", Validators.required],
    asunto: ["Propiedades de Showroom Inmobiliario", Validators.required]
  });

  imagenesForm = this.fb.group({
    img1: [
      "http://s3.amazonaws.com/srinmobiliario-dev/images/files/1/medium-00033FDF-10-99d650a13826da21e6a00c1664485bcc.jpg?1529421456",
      Validators.required
    ],
    img2: [
      "http://s3.amazonaws.com/srinmobiliario-dev/images/files/2/medium-00033FDF-11-088a77052a8a4aa2a253704fc5eb940a.jpg?1529421457",
      Validators.required
    ],
    img3: [
      "http://s3.amazonaws.com/srinmobiliario-dev/images/files/3/medium-00033FDF-12-46af84016e52e0179d8de86b5bf568ed.jpg?1529421457",
      Validators.required
    ],
    img4: [
      "http://s3.amazonaws.com/srinmobiliario-dev/images/files/4/medium-00033FDF-13-94e13ad601e1a3506c65fd867c6399e2.jpg?1529421457",
      Validators.required
    ],
    img5: [
      "http://s3.amazonaws.com/srinmobiliario-dev/images/files/5/medium-00033FDF-14-97e181703c593a5dbda4e38e28e34da7.jpg?1529421457",
      Validators.required
    ],
    img6: [
      "http://s3.amazonaws.com/srinmobiliario-dev/images/files/6/medium-00033FDF-15-43cc3ec54e4a1456bbddbe1f7dd9b270.jpg?1529421457",
      Validators.required
    ]
  });

  constructor(
    private fb: FormBuilder,
    private services: VolantesService,
    private snackBar: MatSnackBar
  ) {}

  onSubmit() {
    this.formDisable = true;
    let body = this.infoForm.value;
    body.imagenes = [
      this.imagenesForm.value.img1,
      this.imagenesForm.value.img2,
      this.imagenesForm.value.img3,
      this.imagenesForm.value.img4,
      this.imagenesForm.value.img5,
      this.imagenesForm.value.img6
    ];

    this.services.enviarInformacion(body).then(
      (resp: any[]) => {
        console.log(resp);
        setTimeout(() => {
          this.formDisable = false;
          this.snackBar.open('Correo enviado');
        }, 2000);
      },
      error => {
        console.log(error);
        setTimeout(() => {
          this.formDisable = false;
        }, 2000);
      }
    );
  }
}
