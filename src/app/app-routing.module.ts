import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OpcionesInicioComponent } from './pages/opciones-inicio/opciones-inicio.component';
import { EnvioCorreoComponent } from './pages/envio-correo/envio-correo.component';
import { ImpresionVolanteComponent } from './pages/impresion-volante/impresion-volante.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: "/opciones",
    pathMatch: "full"
  },
  {
    path: "opciones",
    component: OpcionesInicioComponent
  },
  {
    path: "envio-email",
    component: EnvioCorreoComponent
  },
  {
    path: "impresion-volante",
    component: ImpresionVolanteComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
